Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: spatstat
Upstream-Contact: Adrian Baddeley <adrian.baddeley@uwa.edu.au>
Source: https://cran.r-project.org/package=spatstat

Files: *
Copyright: (C) 2005 - 2016 Adrian Baddeley and Rolf Turner,
           Marie-Colette van Lieshout, Rasmus Waagepetersen, Kasper Klitgaard
           Berthelsen, Dominic Schuhmacher, Ang Qi Wei, C. Beale, B. Biggerstaff,
           R. Bivand, F. Bonneu, J.B. Chen, Y.C. Chin, M. de la Cruz, P.J. Diggle,
           S. Eglen, A. Gault, M. Genton, P. Grabarnik, C. Graf, J. Franklin,
           U. Hahn, M. Hering, M.B. Hansen, M. Hazelton, J. Heikkinen, K. Hornik,
           R. Ihaka, R. John-Chandran, D. Johnson, J. Laake, J. Mateu, P. McCullagh,
           X.C. Mi, J. Moller, L.S. Nielsen, E. Parilov, J. Picka, M. Reiter,
           B.D. Ripley, B. Rowlingson, J. Rudge, A. Sarkka, K. Schladitz,
           B.T. Scott, I.-M. Sintorn, M. Spiess, M. Stevenson, P. Surovy,
           B. Turlach, A. van Burgel, H. Wang and S. Wong.
License: GPL-2+
Comment: Regarding Files data/*.rda
 Date: Mon, 27 May 2013 09:27:10 +0800
 From: Adrian Baddeley <adrian.baddeley@uwa.edu.au>
 Subject: Re: Demo data sets in spatstat R package
 .
 Yes, I could provide original data for each of the datasets in spatstat,
 but may I suggest it would be easier and more reliable to use R
 to convert the compressed binary *.rda files back into readable text files.
 .
 Example (in the R interpreter)
            a <- load("amacrine.rda")
            dump(a, file="amacrine.R")
 .
 Here 'a' is a vector of strings containing the names of the R objects
 that were in the data file.
 .
 'amacrine.R' is a text file that can replace 'amacrine.rda' in the data/
 subdirectory.
 .
 I guess there may even be a way to do this automatically using the R package
 builder;  for that you'd have to ask cran@r-project.org. Currently the package
 builder/checker forces all data files into the compressed .rda format so
 presumably this could be undone.
 .
 All the datasets in spatstat are covered by GPL >= 2
 .
 regards
 Adrian

Files: debian/*
Copyright: 2013-2016 Andreas Tille <tille@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-2'.
